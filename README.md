Moved to https://github.com/votecube/

# votecube-ui-non-read

Starting repository where all votecube UI driven non-read related logic goes.  May be split up later into more fine grained repositories